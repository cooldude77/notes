package com.instanect.instanectnotesmodule.feature.notes.activity.details.presenter.interfaces

import com.instanect.instanectnotesmodule.common.business.objects.NoteObject

interface NoteDetailsActivityPresenterResponseInterface {
    fun onNoteDetailsProcessComplete(noteObject: NoteObject?)
}