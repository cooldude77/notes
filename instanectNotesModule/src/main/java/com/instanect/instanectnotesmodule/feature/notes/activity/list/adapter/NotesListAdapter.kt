package com.instanect.notesListAndTasks.feature.notes.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.instanect.instanectnotesmodule.R
import com.instanect.instanectnotesmodule.common.business.objects.NoteObject


class NotesListAdapter(
    private val arrayNoteObject: Array<NoteObject>,
    private val onItemClickListener: OnItemClickListener
) :
    RecyclerView.Adapter<NotesListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem = layoutInflater.inflate(R.layout.notes_list_item_view, parent, false)
        return ViewHolder(listItem)
    }

    override fun getItemCount(): Int {
        return arrayNoteObject.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val noteObject = arrayNoteObject[position]
        holder.textViewNote.text = noteObject.note
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        var textViewNote: TextView = itemView.findViewById(R.id.textView)
        override fun onClick(view: View?) {
            if (view != null) {
                onItemClickListener.onItemClick(
                    null,
                    view,
                    adapterPosition,
                    view.id.toLong()
                )
            }
        }

    }


}