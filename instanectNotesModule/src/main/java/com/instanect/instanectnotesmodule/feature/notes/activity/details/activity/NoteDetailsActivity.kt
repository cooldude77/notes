package com.instanect.instanectnotesmodule.feature.notes.activity.details.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.instanect.aks_mvp.mvp.alert.interfaces.GenericAlertDialogResponseInterface
import com.instanect.instanectnotesmodule.R
import com.instanect.instanectnotesmodule.common.business.objects.NoteObject
import com.instanect.instanectnotesmodule.feature.notes.activity.details.alert.NoteDetailsAlertFactory
import com.instanect.instanectnotesmodule.feature.notes.activity.details.alert.NoteDetailsAlertFactory.ALERT_CODE_SAVE_NOTE
import com.instanect.instanectnotesmodule.feature.notes.activity.details.fragment.NoteDetailsFragment
import com.instanect.instanectnotesmodule.feature.notes.activity.details.fragment.interfaces.NotesDetailsFragmentResponseInterface
import com.instanect.instanectnotesmodule.feature.notes.activity.details.presenter.NotesDetailsActivityPresenter
import com.instanect.instanectnotesmodule.feature.notes.activity.details.presenter.interfaces.NoteDetailsActivityPresenterResponseInterface
import com.instanect.instanectnotesmodule.feature.notes.activity.details.view.NoteDetailsActivityView
import javax.inject.Inject


open class NoteDetailsActivity : AppCompatActivity(),
    NotesDetailsFragmentResponseInterface,
    NoteDetailsActivityPresenterResponseInterface,
    GenericAlertDialogResponseInterface {

    @Inject
    var notesDetailsActivityPresenter: NotesDetailsActivityPresenter? = null
    @Inject
    var noteDetailsAlertFactory: NoteDetailsAlertFactory? = null
    @Inject
    var noteDetailsActivityView: NoteDetailsActivityView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.generic_activity)


        // Find the toolbar view inside the activity layout
        // Find the toolbar view inside the activity layout
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        // Sets the Toolbar to act as the ActionBar for this Activity window.
// Make sure the toolbar exists in the activity and is not null
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val fragment = NoteDetailsFragment.newInstance(this)

        supportFragmentManager.beginTransaction()
            .replace(
                R.id.fl_generic_fragment,
                fragment
            )
            .commit()

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                noteDetailsAlertFactory?.alertSaveNoteUponExit(this)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onPositiveButtonClicked(alertCode: Int, t: Any?) {

        if (alertCode == ALERT_CODE_SAVE_NOTE)
            notesDetailsActivityPresenter?.save(
                noteDetailsActivityView?.noteId,
                noteDetailsActivityView?.noteTitle,
                noteDetailsActivityView?.noteText
            )
    }

    override fun onNegativeButtonClicked(alertCode: Int, t: Any?) {
        if (alertCode == ALERT_CODE_SAVE_NOTE)
            finish()
    }

    override fun onNoteDetailsProcessComplete(noteObject: NoteObject?) {
        finish()
    }
}
