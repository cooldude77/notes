package com.instanect.instanectnotesmodule.feature.notes.activity.details.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.instanect.instanectnotesmodule.R
import com.instanect.instanectnotesmodule.feature.notes.activity.details.fragment.interfaces.NotesDetailsFragmentResponseInterface

class NoteDetailsFragment : Fragment() {

    lateinit var notesDetailsFragmentResponseInterface: NotesDetailsFragmentResponseInterface

    companion object {

        fun newInstance(notesDetailsFragmentResponseInterface: NotesDetailsFragmentResponseInterface): NoteDetailsFragment {

            val fragment = NoteDetailsFragment()

            fragment.notesDetailsFragmentResponseInterface = notesDetailsFragmentResponseInterface

            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        return inflater.inflate(R.layout.notes_details, null)
    }
}