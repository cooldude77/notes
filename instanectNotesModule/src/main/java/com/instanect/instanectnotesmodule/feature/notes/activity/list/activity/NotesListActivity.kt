package com.instanect.instanectnotesmodule.feature.notes.activity.list.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import com.instanect.instanectnotesmodule.R
import com.instanect.instanectnotesmodule.feature.notes.activity.details.activity.NoteDetailsActivity
import kotlinx.android.synthetic.main.generic_activity.toolbar
import kotlinx.android.synthetic.main.generic_activity_with_fab.*

open class NotesListActivity : AppCompatActivity() {


    private val REQUEST_CODE_NOTE: Int = 1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.generic_activity_with_fab)
        setSupportActionBar(toolbar)

        fab.setOnClickListener {
            //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            //  .setAction("Action", null).show()
            val intent = Intent(this, NoteDetailsActivity::class.java)
            startActivityForResult(intent, REQUEST_CODE_NOTE)
        }


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_list, menu)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_NOTE) {
            if (resultCode == Activity.RESULT_OK)
                return

        }

    }
}