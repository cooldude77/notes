package com.instanect.instanectnotesmodule.feature.notes.activity.details.alert;

import com.instanect.aks_mvp.mvp.alert.GenericAlertDialogBuilder;
import com.instanect.aks_mvp.mvp.alert.interfaces.GenericAlertDialogResponseInterface;

public class NoteDetailsAlertFactory {

    public static final int ALERT_CODE_SAVE_NOTE = 1000;

    private GenericAlertDialogBuilder genericAlertDialogBuilder;
    private GenericAlertDialogResponseInterface genericAlertDialogResponseInterface;

    public NoteDetailsAlertFactory(GenericAlertDialogBuilder genericAlertDialogBuilder) {

        this.genericAlertDialogBuilder = genericAlertDialogBuilder;
    }

    public void alertSaveNoteUponExit(GenericAlertDialogResponseInterface
                                              genericAlertDialogResponseInterface) {


        this.genericAlertDialogResponseInterface = genericAlertDialogResponseInterface;
    }
}
