package com.instanect.instanectnotesmodule.feature.notes.activity.details.view;

import com.instanect.aks_mvp.mvp.view.AbstractView;
import com.instanect.aks_mvp.mvp.view.AppViewHelperInterface;

import org.jetbrains.annotations.NotNull;

public class NoteDetailsActivityView extends AbstractView {
    public NoteDetailsActivityView(AppViewHelperInterface appViewHelperInterface) {
        super(appViewHelperInterface);
    }

    @NotNull
    public Object getNoteId() {
        return null;
    }

    @NotNull
    public Object getNoteTitle() {
        return null;
    }

    @NotNull
    public Object getNoteText() {
        return null;
    }
}
