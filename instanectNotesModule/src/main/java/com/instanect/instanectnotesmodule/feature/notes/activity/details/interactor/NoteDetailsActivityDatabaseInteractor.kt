package com.instanect.instanectnotesmodule.feature.notes.activity.details.interactor

import com.instanect.aks_mvp.mvp.interactors.database.AbstractDatabaseInteractor
import com.instanect.aks_mvp.mvp.interactors.database.builder.DatabaseApiInterface
import com.instanect.aks_mvp.mvp.interactors.database.interfaces.DatabaseObjectInterface
import com.instanect.instanectnotesmodule.common.business.objects.NoteObject
import com.instanect.instanectnotesmodule.common.database.api.NotesApi

class NoteDetailsActivityDatabaseInteractor(databaseApiInterface: DatabaseApiInterface?) :
    AbstractDatabaseInteractor(databaseApiInterface) {
    override fun <T> save(databaseObjectInterface: DatabaseObjectInterface) {
        databaseApiInterface.save(databaseObjectInterface as NoteObject)
        databaseInteractorResponseInterface.onDatabaseInteractorProcessSuccess(
            databaseObjectInterface
        )
    }

    override fun getDatabaseApiInterface(): NotesApi {
        return super.getDatabaseApiInterface() as NotesApi
    }
}