package com.instanect.instanectnotesmodule.common.database.api.model.interfaces

import android.content.ContentValues

interface ModelClassInterface {

    fun getSQLStatement(): String
    fun add(contentValues: ContentValues): Boolean
    fun update(contentValues: ContentValues): Boolean
    fun delete(contentValues: ContentValues): Boolean
}
