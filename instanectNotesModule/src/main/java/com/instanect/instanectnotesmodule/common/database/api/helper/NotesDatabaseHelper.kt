package com.instanect.instanectnotesmodule.common.database.api.helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase

class NotesDatabaseHelper(
    context: Context?,
    name: String?,
    version: Int,
    openParams: SQLiteDatabase.OpenParams
)
