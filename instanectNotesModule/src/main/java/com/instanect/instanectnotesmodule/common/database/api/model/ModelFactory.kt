package com.instanect.instanectnotesmodule.common.database.api.model

import com.instanect.instanectnotesmodule.common.database.api.model.interfaces.ModelClassInterface

class ModelFactory {

    fun <T : ModelClassInterface> newInstance(tClass: Class<out ModelClassInterface>): T {

        try {
            return tClass.newInstance() as T
        } catch (e: Exception) {
            throw AssertionError(e)
        }

    }

}


