package com.instanect.instanectnotesmodule.common.database.api.model

import android.content.ContentValues
import com.instanect.instanectnotesmodule.common.database.api.contentProvider.NotesContentProvider
import com.instanect.instanectnotesmodule.common.database.api.model.interfaces.ModelClassInterface


const val FIELD_ID_NOTE = "_id"
const val FIELD_NOTE_TITLE = "title"
const val FIELD_NOTE_DESC = "description"
const val FIELD_CREATED_AT = "createdAt"
const val FIELD_UPDATED_AT = "updatedAt"

class ModelClassNote(val contentProvider: NotesContentProvider) : ModelClassInterface {


    override fun getSQLStatement(): String {

        return ("CREATE TABLE IF NOT EXISTS User("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "title TEXT NOT NULL,"
                + "description TEXT,"
                + "createdAt TEXT NOT NULL," +
                "updatedAt TEXT NOT NULL,"
                + ")")

    }

    override fun add(contentValues: ContentValues): Boolean {

        //contentProvider.insert();
        return true
    }

    override fun update(contentValues: ContentValues): Boolean {
        return true
    }

    override fun delete(contentValues: ContentValues): Boolean {
        return true
    }
}
