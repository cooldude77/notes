package com.instanect.instanectnotesmodule.common.business.objects

import com.instanect.aks_mvp.mvp.interactors.database.interfaces.DatabaseObjectInterface

class NoteObject : DatabaseObjectInterface {

    var idNotes = -1
    var note = null
    var createdOn = null
    var updateOn = null

}