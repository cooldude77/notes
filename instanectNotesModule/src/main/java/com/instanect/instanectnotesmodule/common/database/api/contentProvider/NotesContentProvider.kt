package com.instanect.instanectnotesmodule.common.database.api.contentProvider

import android.content.ContentProvider
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri
import com.instanect.instanectnotesmodule.common.database.api.helper.NotesDatabaseHelper

class NotesContentProvider : ContentProvider() {
    private var dbsqLiteHelper: NotesDatabaseHelper? = null
    private var contentResolver: ContentResolver? = null
    internal var context: Context? = null

    override fun onCreate(): Boolean {
        context = getContext()
        //  contentResolver = context.getContentResolver()
        //dbsqLiteHelper = NotesDatabaseHelper(context)
        return true
    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        //val db = dbsqLiteHelper.readableDatabase

        val queryBuilder = SQLiteQueryBuilder()

        return null
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        return null
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        return 0
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<String>?
    ): Int {
        return 0
    }
}
