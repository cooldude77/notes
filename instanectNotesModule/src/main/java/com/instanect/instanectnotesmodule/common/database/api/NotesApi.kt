package com.instanect.instanectnotesmodule.common.database.api

import com.instanect.aks_mvp.mvp.interactors.database.builder.DatabaseApiInterface
import com.instanect.instanectnotesmodule.common.business.objects.NoteObject

class NotesApi : DatabaseApiInterface {
    fun save(noteObject: NoteObject?) {}
}