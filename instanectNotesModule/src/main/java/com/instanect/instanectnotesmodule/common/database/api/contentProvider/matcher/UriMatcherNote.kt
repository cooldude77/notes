package com.instanect.instanectnotesmodule.common.database.api.contentProvider.matcher

import android.content.UriMatcher

class UriMatcherNote(uriMatcher: UriMatcher, providerName: String) :
    AbstractUriMatcher(uriMatcher, providerName) {

    override fun add() {
        addToMatcher("note/#", NOTE_WITH_ID)
    }

    companion object {
        val NOTE_WITH_ID = 10
    }
}
