package com.instanect.instanectnotesmodule.common.database.api.contentProvider.matcher

import android.content.UriMatcher
import com.instanect.instanectnotesmodule.common.database.api.contentProvider.matcher.interfaces.UriMatcherInterface
import java.util.*

abstract class AbstractUriMatcher(
    private val uriMatcher: UriMatcher,
    private val providerName: String
) : UriMatcherInterface {

    internal var uriMatcherObjects: ArrayList<UriMatcherObject>? = null

    protected fun addToMatcher(path: String, code: Int) {

        uriMatcher.addURI(providerName, path, code)
    }


}
